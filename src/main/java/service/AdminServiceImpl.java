package service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import entity.Admin;
import entity.LayRequest;
import mapper.AdminMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class AdminServiceImpl implements AdminService{
    @Autowired
    private AdminMapper adminMapper;
    @Override
    public boolean insert(Admin admin) {
        return adminMapper.insert(admin)>0?true:false;
    }

    @Override
    public Admin selectById(Integer id) {
        return adminMapper.selectById(id);
    }

    @Override
    public Admin selectByAccount(String account) {
        return adminMapper.selectByAccount(account);
    }

    @Override
    public boolean delete(Integer id) {
        return adminMapper.delete(id)>0?true:false;
    }

    @Override
    public boolean update(Admin admin) {
        admin.setUpdatetime(new Date());
        return adminMapper.update(admin)>0?true:false;
    }

    /**
     * 分页查询，通过分页插件来实现
     * @param map 查询条件
     * @return
     */
    @Override
    public PageInfo<Admin> selectByPage(Map<String, Object> map) {
        int pageNum=map.get("pageNum")==null?1
                :Integer.valueOf(map.get("pageNum").toString());
        int pageSize=map.get("pageSize")==null?1
                :Integer.valueOf(map.get("pageSize").toString());
        //1.设置当前页和每页记录条数
        PageHelper.startPage(pageNum,pageSize);
        //2.执行查询，startPage之后的第一个查询默认会进行分页
//        List<Admin> admins = adminMapper.selectByMap(map);
        List<Admin> admins = adminMapper.selectWithRole(map);
        //3.创建PageInfo对象，并将返回结果传入。会自动赋值总页数，总记录数当前页等数据
        PageInfo<Admin> adminPageInfo = new PageInfo<>(admins);
        return adminPageInfo;
    }

    @Override
    public PageInfo<Admin> selectByPage(LayRequest layRequest) {
        //1.设置当前页和每页记录条数
        PageHelper.startPage(layRequest.getPage(),layRequest.getLimit());
        //2.执行查询，startPage之后的第一个查询默认会进行分页
        List<Admin> admins = adminMapper.selectWithRole(layRequest.getParams());
        //3.创建PageInfo对象，并将返回结果传入。会自动赋值总页数，总记录数当前页等数据
        PageInfo<Admin> adminPageInfo = new PageInfo<>(admins);
        return adminPageInfo;
    }

    @Override
    public boolean updateHeadPic(Admin admin) {
        return adminMapper.updateHeadPic(admin)>0?true:false;
    }

}
