package service;

import entity.Article;

public interface ArticleService {
    boolean insert(Article article);
}
