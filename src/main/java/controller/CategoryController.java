package controller;

import entity.Category;
import entity.MyResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import service.CategoryService;

import java.util.List;

@RestController//@Controller+@ResponseBody 该类中的所有控制层方法都返回json数据。
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    @GetMapping("/childCategory")
    public MyResult<List<Category>> findChildCategory(Integer parentid){
        List<Category> categories = categoryService.selectByParentId(parentid);
        return new MyResult<>(200,"查询成功",categories);
    }
}
