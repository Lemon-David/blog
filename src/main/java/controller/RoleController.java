package controller;

import aop.RequiredPermission;
import com.github.pagehelper.PageInfo;
import entity.LayRequest;
import entity.MyResult;
import entity.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import service.RoleService;

import java.util.List;

@Controller
@RequestMapping("/role")//设定该类中所有请求的统一访问前缀
public class RoleController {
    //注入service层对象
    @Autowired
    private RoleService roleService;

    @RequestMapping("/add")
    public String add(Role role){
        System.out.println("进入控制层方法");
        boolean flag = roleService.insert(role);
        //重定向 "redirect:重定向的请求地址"; 通常重定向到Controller方法
        return "redirect:list";//重定向到list请求
    }

    @RequiredPermission("role:add")
    @ResponseBody
    @RequestMapping("/add2")
    public MyResult add2(Role role){
        System.out.println("进入控制层方法");
        boolean flag = roleService.insert(role);
        //重定向 "redirect:重定向的请求地址"; 通常重定向到Controller方法
        return new MyResult<>(0);//重定向到list请求
    }

    /**
     * 跳转到对应的添加页面
     * @return
     */
    @RequestMapping("/toAdd")
    public String toAdd(){
        return "add.jsp";
    }

    @RequiredPermission("role:list")
    @RequestMapping("/list")
    public String list(ModelMap modelMap){
        //查询所有角色
        List<Role> roles = roleService.selectAll();
        //将角色集合传递到前台页面
        modelMap.addAttribute("roles",roles);
        return "list.jsp";//转发到对应的页面
    }

    @ResponseBody
    @RequestMapping("/delete")
    public MyResult delete(Integer id){
        boolean flag=roleService.delete(id);
        return new MyResult<>(0);
    }

    @RequestMapping("/toUpdate")
    public String toUpdate(Integer id,ModelMap modelMap){
        //查询角色信息
        Role role=roleService.selectById(id);
        //将角色信息传递到页面
        modelMap.addAttribute("role",role);
        return "update.jsp";
    }

    @RequiredPermission("role:update")
    @RequestMapping("/update")
    public String update(Role role){
        //todo 调用Service 更新角色信息
        boolean flag=roleService.update(role);
        return "redirect:list";
    }

    @ResponseBody
    @RequestMapping("/update2")
    public MyResult update2(Role role){
        //todo 调用Service 更新角色信息
        boolean flag=roleService.update(role);
        return new MyResult<>(0);
    }

    @RequiredPermission("role:list")
    @ResponseBody
    @GetMapping("/layList")
    public MyResult<List<Role>> list(LayRequest layRequest){
        PageInfo<Role> rolePageInfo = roleService.selectByPage(layRequest);
        return new MyResult<>(0,rolePageInfo.getTotal(),rolePageInfo.getList());//转发到对应的页面
    }

    @ResponseBody
    @PostMapping("/batchDelete")
    public MyResult batchDelete(Integer[] ids){
        roleService.batchDelete(ids);
        return new MyResult(0);
    }

}
