package entity;

import java.util.Map;

/**
 * 接受layui表格请求数据的对象
 */
public class LayRequest {
    //当前页
    private int page;
    //每页记录条数
    private int limit;
    //其他查询条件
    private Map<String,Object> params;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }
}
