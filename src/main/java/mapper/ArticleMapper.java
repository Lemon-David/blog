package mapper;

import entity.Article;

public interface ArticleMapper {
    int insert(Article article);
}
