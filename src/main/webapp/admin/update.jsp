<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Lemon
  Date: 2020/6/9
  Time: 14:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>管理员修改</title>
</head>
<body>
<form action="${pageContext.request.contextPath}/admin/update" method="post">
    ID：<input type="text" name="id" value="${admin.id}" readonly><br>
    账号：<input type="text" name="account" value="${admin.account}"><br>
    密码：<input type="password" name="password" value="${admin.password}"><br>
    姓名：<input type="text" name="name" value="${admin.name}"><br>
    手机号：<input type="text" name="phone" value="${admin.phone}"><br>
    邮箱：<input type="text" name="email" value="${admin.email}"><br>
    状态：<input type="radio" name="status" <c:if test="${admin.status}=='1'">checked</c:if> value="1">正常
         <input type="radio" name="status" <c:if test="${admin.status}=='0'">checked</c:if> value="0">注销<br>
    性别：<select name="sex">
            <option <c:if test="${admin.sex}=='1'">selected</c:if> value="1">男</option>
            <option <c:if test="${admin.sex}=='2'">selected</c:if> value="2">女</option>
         </select><br>
            <%--显示后台查询到的所有的角色--%>
    角色：<select name="roleid">
            <c:forEach items="${roles}" var="role">
                <option value="${role.id}">${role.rolename}</option>
            </c:forEach>
         </select>
    <input type="hidden" name="operator" value="1">
    <input type="submit" value="提交">
</form>
</body>
</html>
