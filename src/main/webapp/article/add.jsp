<%--
  Created by IntelliJ IDEA.
  User: Lemon
  Date: 2020/6/15
  Time: 16:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>文章新增</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css">
</head>
<body>
<form class="layui-form">
    <%--一个表单项--%>
    <div class="layui-form-item">
        <label class="layui-form-label">一级菜单</label>
        <div class="layui-input-inline">
            <%--lay-filter="one" 相当于layui中的ID--%>
            <select id="one" lay-filter="one">

            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">二级菜单</label>
        <div class="layui-input-inline">
            <select id="two" name="category">

            </select>
        </div>
    </div>
    <%--一个表单项--%>
    <div class="layui-form-item">
        <label class="layui-form-label">标题</label>
        <%--layui-input-inline 行内 block 占整行--%>
        <div class="layui-input-inline">
            <input type="text" name="title" lay-verify="required" placeholder="请输入标题" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">作者</label>
        <div class="layui-input-inline">
            <input type="text" name="author" lay-verify="required" placeholder="请输入作者" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">关键字</label>
        <div class="layui-input-inline">
            <input type="text" name="keywords" lay-verify="required" placeholder="请输入关键字" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">状态</label>
        <div class="layui-input-inline">
            <input type="radio" name="status" lay-verify="required" value="0" title="不发布" checked>
            <input type="radio" name="status" lay-verify="required" value="1" title="发布">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">操作人</label>
        <%--layui-input-inline 行内 block 占整行--%>
        <div class="layui-input-inline">
            <%--lay-verify="number" 必须是数字--%>
            <input type="text" name="operator" lay-verify="number" value="${sessionScope.admin.id}" placeholder="操作人" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">内容</label>
        <div class="layui-input-block">
            <textarea id="content" name="content"></textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <input type="button" style="margin-left: 100px" lay-filter="add" lay-submit class="layui-btn" value="提交">
    </div>
</form>
    <%----%>
    <%--<textarea id="content" style="width: 500px;height: 300px"></textarea>--%>
<script src="${pageContext.request.contextPath}/static/layui/layui.js"></script>
<script>
    layui.use(['layedit','form','jquery'], function(){
        var layedit = layui.layedit;
        var form=layui.form;
        var $=layui.$;
        layedit.set({
            uploadImage: {//图片上传的接口
                url: '${pageContext.request.contextPath}/article/uploadImg' //接口url
                ,type: 'post' //默认post
            },
            height:200
        });
        var index = layedit.build('content'); //建立编辑器


        $.get("${pageContext.request.contextPath}/category/childCategory",{parentid:-1},function (result) {
            if (result.code==200){
                var html="<option value=''>请选择</option>";
                //获取数据，并展示到下拉列表
                for (var i = 0; i < result.data.length; i++) {
                    html+="<option value='"+result.data[i].id+"'>"+result.data[i].name+"</option>"
                }
                //将内容添加到一级菜单下拉列表
                $("#one").html(html);
                //修改了表单元素之后，需要重新渲染
                form.render(); //更新全部
            }
        },"json")

        /*//select框监听事件
        form.on('select(one)', function(data){
            console.log(data.elem); //得到select原始DOM对象
            console.log(data.value); //得到被选中的值
            console.log(data.othis); //得到美化后的DOM对象
        });*/
        //select框监听事件
        form.on('select(one)', function(data){//回调函数
            if (data.value==''){//如果是请选择，则将二级菜单清空
                $("#two").empty();
                form.render();
                return;
            }
            $.get("${pageContext.request.contextPath}/category/childCategory",{parentid:data.value},function (result) {
                if (result.code==200){
                    var html="<option value=''>请选择</option>";
                    //获取数据，并展示到下拉列表
                    for (var i = 0; i < result.data.length; i++) {
                        html+="<option value='"+result.data[i].id+"'>"+result.data[i].name+"</option>"
                    }
                    //将内容添加到一级菜单下拉列表
                    $("#two").html(html);
                    //修改了表单元素之后，需要重新渲染
                    form.render(); //更新全部
                }
            },"json")
        });

        form.on("submit(add)",function (data) {
            //将内容同步到textarea
            layedit.sync(index);
            //获取富文本内容
            var text=layedit.getText(index);
            data.field.content=text;
            console.log(data.field);
            //获取表单数据项
            console.log($("form").serialize());

            $.post("${pageContext.request.contextPath}/article/save",
                $("form").serialize(),
                function (result) {
                    if (result.code==0){
                        layer.msg("保存成功")
                    }
            })
            return false;
        })
    });
</script>
</body>
</html>
