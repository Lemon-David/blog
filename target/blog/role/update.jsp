<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Lemon
  Date: 2020/6/8
  Time: 16:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>角色编辑</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css">
</head>
<body>
<%--layui-form layui的表单样式，添加到form表单上--%>
<form class="layui-form" method="post">
    <%--一个表单项--%>
    <div class="layui-form-item">
        <label class="layui-form-label">角色名</label>
        <%--layui-input-inline 行内 block 占整行--%>
        <div class="layui-input-inline">
            <input type="text" name="rolename" value="${role.rolename}" placeholder="请输入角色名" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">状态</label>
        <%--layui-input-inline 行内 block 占整行--%>
        <div class="layui-input-inline">
            <input type="radio" name="status" value="1" <c:if test="${role.status=='1'}">checked</c:if> title="正常">
            <input type="radio" name="status" value="0" <c:if test="${role.status=='0'}">checked</c:if> title="注销">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">操作人</label>
        <%--layui-input-inline 行内 block 占整行--%>
        <div class="layui-input-inline">
            <input type="text" name="operator" value="${role.operator}" placeholder="操作人" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">描述</label>
        <div class="layui-input-inline">
            <textarea type="text" name="explain" class="layui-textarea">${role.explain}</textarea>
        </div>
    </div>
    <input type="hidden" value="${role.id}" name="id" >
    <div class="layui-form-item">
        <%--lay-submit 标识该按钮可以提交表单--%>
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="update">立即提交</button>
            <button class="layui-btn">重置</button>
        </div>
        <%--<input type="submit" style="margin-left: 100px" lay-filter="update" lay-submit class="layui-btn" value="提交">--%>
    </div>
</form>
<%--引入js--%>
<script src="${pageContext.request.contextPath}/static/layui/layui.js"></script>
<script>
    //layui引入特定模块的方式
    layui.use(['layer', 'form','jquery'], function(){
        var layer = layui.layer//layer 弹窗对象
            ,form = layui.form;//表单对象
        var $=layui.$;
        // layer.msg('Hello World');
        form.on("submit(update)",function (data) {
            $.post("${pageContext.request.contextPath}/role/update2",data.field,function (result) {
                if (result.code==0){
                    layer.msg("修改成功")
                    //刷新父窗口内容
                    parent.layui.table.reload("roleTable",{
                        page:{
                            curr:1
                        }
                    })
                    //关闭弹出
                    var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                    parent.layer.close(index); //再执行关闭
                }
            },"json");
            return false;//阻止表单提交
        })
    });
</script>
</body>
</html>
