<%--
  Created by IntelliJ IDEA.
  User: Lemon
  Date: 2020/6/11
  Time: 14:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>角色管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css"  media="all">
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<body>
    <%--头部多条件查询--%>
    <form class="layui-form">
        <div class="layui-inline">
            <label class="layui-form-label">角色名</label>
            <div class="layui-input-inline">
                <input type="text" name="rolename" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">状态</label>
            <div class="layui-input-inline">
                <select name="status">
                    <option value="">请选择</option>
                    <option value="1">正常</option>
                    <option value="0">注销</option>
                </select>
            </div>
        </div>
        <div class="layui-inline">
            <div class="layui-input-block">
                <button class="layui-btn" lay-filter="query" lay-submit>查询</button>
            </div>
        </div>
    </form>
    <table class="layui-hide" id="roleTable" lay-filter="roleTable"></table>
    <%--html模板，别的地方可以根据ID来引用该模板内容--%>
    <script type="text/html" id="roleToolbar">
        <div class="layui-btn-container"><%--定义一个按钮组--%>
            <button class="layui-btn layui-btn-sm" lay-event="add">新增</button>
            <button class="layui-btn layui-btn-sm" lay-event="delete">删除</button>
        </div>
    </script>

    <script type="text/html" id="innerToolBar">
        <div class="layui-btn-container"><%--定义一个按钮组--%>
            <a class="layui-btn layui-btn-xs" lay-event="edit">修改</a>
            <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="delete">删除</a>
        </div>
    </script>
<%--引入js--%>
<script src="${pageContext.request.contextPath}/static/layui/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script>
    //layui引入特定模块的方式
    layui.use(['layer', 'form','table','jquery'], function(){
        var layer = layui.layer//layer 弹窗对象
            , form = layui.form;//表单对象
        var table=layui.table;
        var $=layui.$;//引入jquery
        //表单提交事件监听 submit(触发submit的按钮的lay-filter)
        form.on('submit(query)',function (data) {
            console.log(data.elem)//被执行事件的元素DOM对象，一般为button对象
            console.log(data.form)//被执行提交的form对象，一般在存在form标签时才会返回
            console.log(data.field)//当前容器的全部表单字段，名值对形式：{name:value}
            //重新加载表格
            table.reload("roleTable",{
                where:{//查询条件
                    params:data.field//会封装到LayRequest中的params中去
                },
                page:{
                    curr:1//当前页
                }
            })
            return false;//阻止表单跳转。如果需要表单跳转，去掉这段即可。
        })
        table.render({
            elem: '#roleTable'
            ,url:'${pageContext.request.contextPath}/role/layList'
            // ,toolbar: 'default'//让工具栏左侧显示默认的内置模板
            // ,toolbar: '<div>xxx</div>'//直接传入工具栏模板字符
            ,toolbar: '#roleToolbar' //开启头部工具栏，并为其绑定左侧模板 指向自定义工具栏模板选择器
            ,defaultToolbar: ['filter', 'exports', 'print', //右侧工具栏
                { //自定义头部工具栏右侧图标。如无需自定义，去除该参数即可
                title: '提示'
                ,layEvent: 'LAYTABLE_TIPS'
                ,icon: 'layui-icon-tips'
            }]
            ,title: '用户数据表'
            ,cols: [[//定义列信息
                {type: 'checkbox', fixed: 'left'}//最左侧复选框
                ,{field:'id', title:'ID', width:80, fixed: 'left', unresize: true, sort: true}//一列数据项
                ,{field:'rolename', title:'角色名', width:150}
                ,{field:'explain', title:'描述',width:250}
                ,{field:'status', title:'状态', width:150,
                    templet: function(res){//使用模板进行显示转换
                        if (res.status=="1"){//res当前行数据
                            return "正常";
                        }
                        return "注销"
                    }
                }
                ,{field:'operatorAdmin', title:'操作人ID', width:200,
                    templet:function (res) {//取对象属性中的属性
                    //todo 可能会存在一个角色对应多个操作人，页面却只显示一个的问题
                        if (res.operatorAdmin!=null){
                            return res.operatorAdmin.account;
                        }
                        return "";
                    }
                }
                ,{field:'createtime', title:'创建时间', width:250}
                ,{field:'updatetime', title:'更新时间', width:250}
                ,{fixed: 'right', title:'操作', toolbar: '#innerToolBar', width:200}//表格内操作栏
            ]]
            ,page: true
        });

        //头工具栏事件
        table.on('toolbar(test)', function(obj){
            var checkStatus = table.checkStatus(obj.config.id);
            switch(obj.event){
                case 'getCheckData':
                    var data = checkStatus.data;
                    layer.alert(JSON.stringify(data));
                    break;
                case 'getCheckLength':
                    var data = checkStatus.data;
                    layer.msg('选中了：'+ data.length + ' 个');
                    break;
                case 'isAll':
                    layer.msg(checkStatus.isAll ? '全选': '未全选');
                    break;

                //自定义头工具栏右侧图标 - 提示
                case 'LAYTABLE_TIPS':
                    layer.alert('这是工具栏右侧自定义的一个图标按钮');
                    break;
            };
        });

        //监听行工具事件
        table.on('tool(test)', function(obj){
            var data = obj.data;
            //console.log(obj)
            if(obj.event === 'del'){
                layer.confirm('真的删除行么', function(index){
                    obj.del();
                    layer.close(index);
                });
            } else if(obj.event === 'edit'){
                layer.prompt({
                    formType: 2
                    ,value: data.email
                }, function(value, index){
                    obj.update({
                        email: value
                    });
                    layer.close(index);
                });
            }
        });

        //监听头部工具栏事件
        //toolbar(table的lay-filter)
        table.on('toolbar(roleTable)', function(obj){
            var checkStatus = table.checkStatus(obj.config.id);//获取到选中的内容
            switch(obj.event){//工具栏中按钮的lay-event属性的值
                case 'add':
                    //弹出层
                    layer.open({
                        title:"角色新增",//标题
                        type:2,//2 为iframe层，可以加载其他的页面到弹出层中
                        content:"${pageContext.request.contextPath}/role/toAdd",
                        area: ['500px', '400px'],//定义宽高
                        maxmin:true//可以最大化
                    })
                    break;
                case 'delete':
                    layer.msg('删除');
                    console.log(checkStatus)
                    console.log(checkStatus.data)//选中的数据
                    if (checkStatus.data.length==0){
                        layer.alert("请至少选择一项删除！")
                        return;
                    }
                    var data=checkStatus.data;
                    var idArray=[];
                    //获取ID值，并放入数组
                    for (var i = 0; i < data.length; i++) {
                        idArray.push(data[i].id);
                    }
                    console.log(idArray)
                    //执行批量删除
                    $.post("${pageContext.request.contextPath}/role/batchDelete",
                        {ids:idArray.join(",")},function (result) {
                            if (result.code==0){
                                layer.msg("删除成功")
                                //重新加载表格
                                table.reload("roleTable",{
                                    // where:{
                                    //
                                    // },
                                    page:{
                                        curr:1
                                    }
                                })
                                return
                            }
                            layer.msg("删除失败")
                        },"json");
                    break;
            };
        });

        //行内操作栏监听事件
        table.on('tool(roleTable)', function(obj) { //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
            var data = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var tr = obj.tr; //获得当前行 tr 的 DOM 对象（如果有的话）
            switch(layEvent){
                case 'delete':
                    // layer.alert("删除该行记录，id："+data.id)
                    //todo 发ajax请求异步删除数据
                    $.post("${pageContext.request.contextPath}/role/delete?id="+data.id,function (result) {
                        if (result.code==0){
                            layer.msg("删除成功");
                            table.reload("roleTable",{
                                page:{
                                    curr:1,
                                }
                            })
                        }else {
                            layer.msg("删除失败");
                        }
                    },"json");
                    break;
                case 'edit':
                    layer.msg("编辑");
                    layer.open({
                        title:"角色修改",//标题
                        type:2,//2 为iframe层，可以加载其他的页面到弹出层中
                        content:"${pageContext.request.contextPath}/role/toUpdate?id="+data.id,
                        area:['500px','400px'],//定义宽高
                        maxmin:true//可以最大化
                    });
                    break;
            }
            })
    });
</script>

</body>
</html>





